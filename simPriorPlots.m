%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PURPOSE:      Plot realization from hierarchical spatial prior 
%
% AUTHOR:       Mattias Villani
%               Division of Statistics and Machine Learning
%               Department of Computer and Information Science
%               Linkoping University      
%
% FIRST VER.:   2018-09-29
% REVISED:      
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Settings
clear all
close all
clc
cmap = 'default';
fontSizer = 10;
printPath = '~/Dropbox/Apps/ShareLaTeX/BAspatialDiscussion2018/figs/';
codePath = '~/Dropbox/Apps/ShareLaTeX/BAspatialDiscussion2018/figs/';

%% Prelims
cd(codePath)

%% Prior prob figure setup
fig904 = figure(904);
fig904pos = get(fig904,'Position');
 
set(fig904,'Position',fig904pos + [0 0 -0*fig904pos(3) -.6*fig904pos(4)]);

%% Prior probs 1
subplot(1,3,1)
load('simHierSpatialPrior1.mat')
imagesc(probGammaVoxels)
caxis([0 1])
axis image
colormap(cmap);

set(gca,'FontSize', 10, 'YTickLabel', [], 'XTickLabel',[]);
title('\delta=10, r=0.05')
ylabel('Prior incl prob, Pr(\gamma=1)')
colorbar('manual','Position',[.35 .165 .019 .71]);

                         
%% Prior probs 2
subplot(1,3,2);
load('simHierSpatialPrior2.mat')
imagesc(probGammaVoxels)
caxis([0 1])
axis image
colormap(cmap);
title('\delta=2, r=1')
set(gca,'FontSize', 10, 'YTickLabel', [], 'XTickLabel',[]);
colorbar('manual','Position',[.63 .165 .019 .71]);
             
%% Prior probs 3
subplot(1,3,3);
load('simHierSpatialPrior3.mat')
imagesc(probGammaVoxels)
caxis([0 1])
axis image
colormap(cmap);
title('\delta=0.1, r=0.05')
set(gca,'FontSize', 10, 'YTickLabel', [], 'XTickLabel',[]);
colorbar('manual','Position',[.91 .165 .019 .71]);
               
 
%% Tightfig
fig904 = tightfig_mod(fig904);

print simPriorProbs -depsc2









%% gamma figure setup
fig905 = figure(905);
fig905pos = get(fig905,'Position');
 
set(fig905,'Position',fig905pos + [0 0 -0*fig905pos(3) -.6*fig905pos(4)]);

%% Prior probs 1
subplot(1,3,1)
load('simHierSpatialPrior1.mat')
imagesc(gammaVoxels)
caxis([0 1])
axis image
colormap(cmap);
title('\delta=10, r=0.05')
set(gca,'FontSize', 10, 'YTickLabel', [], 'XTickLabel',[]);
ylabel('Activity indicators, \gamma')
colorbar('manual','Position',[.35 .165 .019 .71]);

                         
%% Prior probs 2
subplot(1,3,2);
load('simHierSpatialPrior2.mat')
imagesc(gammaVoxels)
caxis([0 1])
axis image
colormap(cmap);
title('\delta=2, r=1')
set(gca,'FontSize', 10, 'YTickLabel', [], 'XTickLabel',[]);
colorbar('manual','Position',[.63 .165 .019 .71]);
             
%% Prior probs 3
subplot(1,3,3);
load('simHierSpatialPrior3.mat')
imagesc(gammaVoxels)
caxis([0 1])
axis image
colormap(cmap);
title('\delta=0.1, r=0.05')
set(gca,'FontSize', 10, 'YTickLabel', [], 'XTickLabel',[]);
colorbar('manual','Position',[.91 .165 .019 .71]);
               
 
%% Tightfig
fig905 = tightfig_mod(fig905);

print simPriorGamma -depsc2